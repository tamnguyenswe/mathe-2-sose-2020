#include <iostream>
#include <cmath>
#include "CMyMatrix.h"

CMyVektor f(CMyVektor x) {
    const int VECTOR_DIMENSION = 2;
    if (x.get_dimension() != VECTOR_DIMENSION)
        throw VectorException::WrongDimensionException();

    std::vector<double> result = std::vector<double>(2);

    result[0] = 3 * x[0] * x[0] + 5 * x[1] - 7;
    result[1] = 7 * x[0] + 3 * x[1] * x[1] - 12;

    return CMyVektor(result);
}

CMyVektor f2(CMyVektor x) {
    const int VECTOR_DIMENSION = 4;
    if (x.get_dimension() != VECTOR_DIMENSION)
        throw VectorException::WrongDimensionException();

    std::vector<double> result = std::vector<double>(3);

    result[0] = x[0] * x[1] * exp(x[2]);
    result[1] = x[1] * x[2] * x[3];
    result[2] = x[3];

    return CMyVektor(result);
}

CMyVektor newton(const CMyVektor& x0, CMyVektor (*f) (CMyVektor)) {
    const int LOOP_LIMIT = 50;
    const double VALUE_LIMIT = 1e-5;

    CMyVektor x = x0;

    for(int count = 0; count < LOOP_LIMIT; count++) {
        std::cout << "Step " << count << ":\n";
        std::cout << "      x = " <<x << "\n";
        std::cout << "      f(x) = " << f(x) << "\n";

        CMyVektor dx = jacobi(x, f).invert() * (-f(x));

        std::cout << "      dx = " << dx << "\n";
        std::cout << "      |f(x)| = " << f(x).get_length() << "\n\n";
        x = x + dx;
        if (f(x).get_length() < VALUE_LIMIT) {
            std::cout << "found:\n";
            std::cout << "      x = " <<x << "\n";
            std::cout << "      f(x) = " << f(x) << "\n";
            std::cout << "      |f(x)| = " << f(x).get_length() << "\n\n";
            break;
        }
    }

    return x;
}


CMyVektor f3(CMyVektor x) {
    const int VECTOR_DIMENSION = 2;
    if (x.get_dimension() != VECTOR_DIMENSION)
        throw VectorException::WrongDimensionException();

    std::vector<double> result = std::vector<double>(2);

    result[0] = x[0] * x[0] * x[0] * x[1] * x[1] * x[1] - 2 * x[1];
    result[1] = x[0] - 2;

    return CMyVektor(result);
}

int main() {
    CMyVektor x = CMyVektor({1, 1});

//    std::cout << jacobi(x, f3);
    CMyVektor dx = jacobi(x, f3).invert() * (-f3(x));
    std::cout << newton(x, f3);

    return 0;
}
