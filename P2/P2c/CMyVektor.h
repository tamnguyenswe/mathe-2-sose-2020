// CMyVektor v3.1: small changes to ease further development. enabling vector subtraction

#ifndef CMYVEKTOR_H
#define CMYVEKTOR_H

#include "VectorException.h"
#include <vector>
#include <cmath>
#include <iostream>

class CMyMatrix;

class CMyVektor {
public:
    CMyVektor(std::vector<double> value) {
        set_dimension(value.size());

        set_value(value);
    }


    CMyVektor(int dimension) {
        set_dimension(dimension);
        this->value.resize(dimension);
    }


    CMyVektor(int dimension, std::vector<double> value) {
        set_dimension(dimension);

        set_value(value);
    }


    /**
     * Set new values to this vector as a double array.
     * If passed array's size is bigger than dimension, the rest will be omitted
     * If passed array's size is smaller than dimension, throw WrongDimensionException
     *
     * @param value This vector's new value
     */
    void set_value(std::vector<double> value) {
        if (this->dimension > value.size()) {
            throw VectorException::WrongDimensionException();
        }
        this->value = value;
        this->is_value_set = true;
    }


    /**
     * Set one value in one certain position
     * @param index target's index
     * @param new_value target's new value
     */
    void set_value_at(int index, double new_value) {
        this->validate_value();
        this->value[index] = new_value;
    }


    /**
     * Set this vector's dimension. Also reset this vector's value
     * @param dimension This vector's new dimension
     */
    void set_dimension(int dimension) {
        validate_dimension(dimension);

        this->dimension = dimension;
        this->value.resize(dimension);
        this->reset_value();
    }


    /**
     * @return This vector's dimension
     */
    int get_dimension() const {
        return this->dimension;
    }


    /**
     * @return this vector's value
     */
    std::vector<double> get_value() const {
        return this->value;
    }


    /**
     * @return This vector's length
     */
    double get_length() const {
        double square_sum = 0;
        for (int i = 0; i < dimension; i++) {
            square_sum += value[i] * value[i];
        }

        return sqrt(square_sum);
    }


    CMyMatrix toMatrix() const; //define later in CMyMatrix.h


    double operator[] (const int i) const {
        this->validate_value();
        this->validate_index(i);

        return this->value[i];
    }


    double &operator[] (const int i) {
        this->validate_index(i);
        return this->value[i];
    }


    operator CMyMatrix() const; //define later in CMyMatrix.h


private:
    std::vector<double> value;
    int dimension;
    bool is_value_set = false;


    void reset_value() {
        this->value.clear();
        this->is_value_set = false;
    }


    /**
     * Throw EmptyValueException if this vector's value is not defined
     */
    void validate_value() const {
        if (!is_value_set) {
            throw VectorException::EmptyValueException();
        }
    }


    /**
     * Throw VectorOutOfBoundException if tries to access non existent values
     */
    void validate_index(int index) const {
        if ((index < 0) || (index >= this->dimension))
            throw VectorException::VectorOutOfBoundException();
    }


    /**
     * Throw InvalidDimensionException if dimension is not valid
     * @param dimension
     */
    void validate_dimension(int dimension) const {
        if (dimension < 1)
            throw VectorException::InvalidDimensionException();
    }
};

CMyVektor operator+ (const CMyVektor& vector_a, const CMyVektor& vector_b) {
    if (vector_a.get_dimension() != vector_b.get_dimension()) {
        throw VectorException::InvalidDimensionException();
    }

    std::vector<double> new_value;
    int new_dimension = vector_a.get_dimension();

    new_value.resize(new_dimension);

    for (int i = 0; i < new_dimension; i++) {
        new_value[i] = vector_a[i] + vector_b[i];
    }

    return CMyVektor(new_dimension, new_value);
}


/**
 * Calculate scalar product of a vector and a real number
 * @param vector
 * @param LAMBDA
 * @return
 */
CMyVektor operator* (CMyVektor vector, const double LAMBDA) {
    for (int i = 0; i < vector.get_dimension(); i++) {
//        vector.set_value_at(i, LAMBDA * vector[i]);
        vector[i] *= LAMBDA;
    }

    return vector;
}


/**
 * Calculate scalar product of a vector and a real number
 * @param LAMBDA
 * @param vector
 * @return
 */
CMyVektor operator* (const double LAMBDA, const CMyVektor& vector) {
    return vector * LAMBDA;
}


CMyVektor operator- (const CMyVektor& vector) {
    return (-1 * vector);
}


CMyVektor operator- (const CMyVektor& vector1, const CMyVektor& vector2) {
    return vector1 + (-vector2);
}


std::ostream& operator<< (std::ostream & out, const CMyVektor vector) {
    out << "( ";
    for (int i = 0; i < vector.get_dimension() - 1; i++) {
        out << vector[i] << " , ";
    }
    out << vector[vector.get_dimension() - 1] << " )";

    return out;
}


/**
 * Calculate gradient of function f at the point x
 * @param x The vector represent point x in hyperspace
 * @return Gradient of function f at the point x
 */
CMyVektor gradient(CMyVektor x, double (*f)(CMyVektor)) {
    const double h = 1e-4;

    double f2 = f(x);

    std::vector<double> gradient_value;

    gradient_value.resize(x.get_dimension());

    for (int i = 0; i < gradient_value.size(); i++) {
        CMyVektor x_h = x;
        x_h[i] =  x_h[i] + h;
        double f1 = f(x_h);
        gradient_value[i] = (f1 - f2) / h;
    }

    return CMyVektor(gradient_value);
}


/**
* Calculate gradient of the multidimensional function f at the point x
* @param x The vector represent point x in hyperspace
* @param f The multidimensional function return a vector
* @param index_f Index of the 1-dimensional mini-function return a double inside f
* @return
*/
CMyVektor gradient(CMyVektor x, CMyVektor (*f)(CMyVektor), int index_f) {
    const double h = 1e-4;

    double f2 = f(x)[index_f];

    std::vector<double> gradient_value;

    gradient_value.resize(x.get_dimension());

    for (int i = 0; i < gradient_value.size(); i++) {
        CMyVektor x_h = x;
        x_h[i] =  x_h[i] + h;
        double f1 = f(x_h)[index_f];
        gradient_value[i] = (f1 - f2) / h;
    }

    return CMyVektor(gradient_value);
}

#endif //CMYVEKTOR_H