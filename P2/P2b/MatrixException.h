//
// Created by presariohg on 04/05/2020.
//

#ifndef MATRIXEXCEPTION_H
#define MATRIXEXCEPTION_H

#include <exception>
class MatrixException {
public:
    struct InvalidMatrixException : public std::exception {
        const char * what() const throw() {
            return "Invalid matrix dimension!";
        }
    };

    struct EmptyMatrixException : public std::exception {
        const char * what() const throw() {
            return "Cannot access an empty matrix!";
        }
    };

    struct MatrixOutOfBoundException : public std::exception {
        const char * what() const throw() {
            return "The matrix does not have these coordinates.";
        }
    };


    struct NonInvertibleMatrixException : public std::exception {
        const char * what() const throw() {
            return "This matrix cannot be inverted with current technology...";
        }
    };


    struct InvalidMatricesDimensionException : public std::exception {
        const char * what() const throw() {
            return "These matrices cannot be multiply with eachother!";
        }
    };


};


#endif //P2A_MATRIXEXCEPTION_H
