#include <iostream>
#include <cmath>
#include "CMyMatrix.h"

CMyVektor f(CMyVektor x) {
    const int VECTOR_DIMENSION = 2;
    if (x.get_dimension() != VECTOR_DIMENSION)
        throw VectorException::WrongDimensionException();

    std::vector<double> result = std::vector<double>(2);

    result[0] = 3 * x[0] * x[0] + 5 * x[1] - 7;
    result[1] = 7 * x[0] + 3 * x[1] * x[1] - 12;

    return CMyVektor(result);
}

CMyVektor f2(CMyVektor x) {
    const int VECTOR_DIMENSION = 4;
    if (x.get_dimension() != VECTOR_DIMENSION)
        throw VectorException::WrongDimensionException();

    std::vector<double> result = std::vector<double>(3);

    result[0] = x[0] * x[1] * exp(x[2]);
    result[1] = x[1] * x[2] * x[3];
    result[2] = x[3];

    return CMyVektor(result);
}

int main() {
//    CMyVektor x = CMyVektor({1, 1});
    CMyVektor x = CMyVektor({1, 2, 0, 3});

    std::cout << jacobi(x, f2);
//    std::cout << jacobi(x, f).invert();

    return 0;
}
