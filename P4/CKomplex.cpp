//
// Created by presariohg on 30/05/2020.
//

#include "CKomplex.h"


CKomplex operator+ (const CKomplex & num1, const CKomplex & num2);


CKomplex operator+ (const CKomplex & num1, const CKomplex & num2) {
    double newRe = num1.real + num2.real;
    double newIm = num1.imaginary + num2.imaginary;

    return {newRe, newIm};
}


CKomplex operator* (const CKomplex & num1, const CKomplex & num2) {
    double newRe = num1.real * num2.real - num1.imaginary * num2.imaginary;
    double newIm = num1.real * num2.imaginary + num1.imaginary * num2.real;

    return {newRe, newIm};
}


CKomplex operator* (const CKomplex & complex, const double & ratio) {
    double newRe = complex.real * ratio;
    double newIm = complex.imaginary * ratio;

    return {newRe, newIm};
}


CKomplex operator* (const double & ratio, const CKomplex & complex) {
    return complex * ratio;
}


std::ostream& operator<< (std::ostream & out, CKomplex num) {
    out << num.real;

    if (num.imaginary < 0)
        out << " - " << std::abs(num.imaginary) << "i";
    else
        out << " + " << num.imaginary << "i";

    return out;
}