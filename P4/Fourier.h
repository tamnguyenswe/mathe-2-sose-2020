//
// Created by presariohg on 29/05/2020.
//


#ifndef FOURIER
#define FOURIER
#include "CKomplex.h"

#define PI 3.14159265358979323846
#include <vector>


/**
 * Sorry for the shitty variable names, but I'd like to follow the names in the vorlesung..
 * @param N f[]'s or c[]' size, depends on normal Fourier transformation or inverse transformation.
 * @return
 */
std::vector<std::vector<CKomplex>> generateWMatrix(const int N, bool isInverse) {
    std::vector<std::vector<CKomplex>> wMatrix;

    wMatrix.resize(N); // N rows
    int param = (isInverse) ? 1 : -1; //inverse transform uses a positive matrix, normal one uses a negative matrix

    for (int n = 0; n < N; n++) {
        wMatrix[n].resize(N); // N columns
        for (int k = 0; k < N; k++)
            wMatrix[n][k] = CKomplex(param * 2 * PI * k * n / N);
    }

    return wMatrix;
}


/**
 * Normal Fourier Transformation
 * @param f Vector f contains fks
 * @return Vector c contains cns
 */
std::vector<CKomplex> fourierTransform(const std::vector<CKomplex> & f) {
    const int N = f.size();
    std::vector<std::vector<CKomplex>> wMatrix = generateWMatrix(N, false);
    std::vector<CKomplex> c;

    c.resize(N);

    //Matrix multiplication
    for (int n = 0; n < N; n++) {
        c[n] = CKomplex(0, 0);
        for (int k = 0; k < N; k++) {
            c[n] = c[n] + f[k] * wMatrix[n][k];
        }
        c[n] = c[n] * (1 / sqrt(N));
    }

    return c;
}


std::vector<CKomplex> fourierInverse(const std::vector<CKomplex> & c) {
    const int N = c.size();
    std::vector<std::vector<CKomplex>> wMatrix = generateWMatrix(N, true);
    std::vector<CKomplex> fComplex;
//    std::vector<double> fReal;

    fComplex.resize(N);
//    fReal.resize(N);

    //Matrix multiplication
    for (int n = 0; n < N; n++) {
        fComplex[n] = CKomplex(0, 0);
        for (int k = 0; k < N; k++) {
            fComplex[n] = fComplex[n] + c[k] * wMatrix[n][k];
        }
        fComplex[n] = fComplex[n] * (1 / sqrt(N));

//        fReal[n] = fComplex[n].re();
    }

//    return fReal;
    return fComplex;
}
#endif