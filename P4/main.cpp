#include <iostream>
#include <vector>
#include <fstream>

#include "CKomplex.h"
#include "Fourier.h"

std::vector<CKomplex>  werte_einlesen(const char *dateiname)
{
    int i, N, idx;
    double re, im;
    std::vector<CKomplex> werte;
    // File oeffnen
    std::ifstream fp;
    fp.open(dateiname);
    // Dimension einlesen
    fp >> N;
    // Werte-Vektor anlegen
    werte.resize(N);
    CKomplex null(0,0);
    for (i = 0; i<N; i++)
        werte[i] = null;
    // Eintraege einlesen und im Werte-Vektor ablegen
    while (!fp.eof())
    {
        fp >> idx >> re >> im;
        CKomplex a(re,im);
        werte[idx] = a;
    }
    // File schliessen
    fp.close();

    return werte;
}


void werte_ausgeben(const char *dateiname, std::vector<CKomplex> werte, double epsilon = -1.0)
{
    int i;
    int N = werte.size();
    // File oeffnen
    std::ofstream fp;
    fp.open(dateiname);
    // Dimension in das File schreiben
    fp << N << '\n';
    // Eintraege in das File schreiben
    fp.precision(10);
    for (i = 0; i < N; i++)
        if (werte[i].abs() > epsilon)
            fp << i << "\t" << werte[i].re() << "\t" << werte[i].im() << '\n';
    // File schliessen
    fp.close();
}


double maxDeviation(const char * inputFile, const char * outputFile, double epsilon = -1.0) {
    std::vector<CKomplex> f = werte_einlesen(inputFile);
    std::vector<CKomplex> c = fourierTransform(f);

    werte_ausgeben(outputFile, c, epsilon);

    std::vector<CKomplex> cNew = werte_einlesen(outputFile);
    std::vector<CKomplex> fNew = fourierInverse(cNew);

    double maxDeviation = 0;
    for (int i = 0; i < f.size(); i++) {
        maxDeviation = std::fmax(maxDeviation, std::abs(f[i].abs() - fNew[i].abs()));
    }

    return maxDeviation;

}

int main() {
    double deviation_default = maxDeviation("in.txt", "out_default.txt");
    double deviation_0_1 = maxDeviation("in.txt", "out_0.1.txt", 0.1);
    double deviation_1_0 = maxDeviation("in.txt", "out_1.0.txt", 1.0);

    std::cout << "Maximum deviation with default epsilon: " << deviation_default << '\n';
    std::cout << "Maximum deviation with epsilon = 0.1: " << deviation_0_1 << '\n';
    std::cout << "Maximum deviation with epsilon = 1.0: " << deviation_1_0 << '\n';

    return 0;

}
