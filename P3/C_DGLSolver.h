//
// Created by presariohg on 11/05/2020.
//

#ifndef P3_C_DGLSOLVER_H
#define P3_C_DGLSOLVER_H

#include "CMyVektor.h"

class C_DGLSolver {
private:
    CMyVektor (*f_DGL_System)(CMyVektor, double);


    double (*f_DGL_nterOrdnung)(CMyVektor, double);


    const bool is_high_order;


    /**
     * Calculate the derivative of this solver's function at (x, y)
     * @return
     */
    CMyVektor ableitungen(const CMyVektor & y, const double & x) {
        if (is_high_order) {
            int dimension = y.get_dimension();

            std::vector<double> y_d = std::vector<double>();
            y_d.resize(dimension);

            // y_d[0 ... n - 1] = y[1...n]
            for (int i = 0; i < dimension - 1; i++) {
                y_d[i] = y[i+1];
            }

            // y_d[n] = f_n_order(y, x);
            y_d[dimension - 1] = f_DGL_nterOrdnung(y, x);

            return CMyVektor(y_d);
        } else {
            return f_DGL_System(y, x);
        }
    }


public:

    /**
     * @param f_DGL_System The DGL system to be solved.
     */
    C_DGLSolver(CMyVektor (*f_DGL_System)(CMyVektor, double)) : is_high_order(false) {
        this->f_DGL_System = f_DGL_System;
    }


    /**
     * @param f_DGL_nterOrdnung The higher order differential equation to be solved
     */
    C_DGLSolver(double (*f_DGL_nterOrdnung)(CMyVektor, double)) : is_high_order(true) {
        this->f_DGL_nterOrdnung = f_DGL_nterOrdnung;
    }


    /**
     * Find the approximate y(x_end) using Euler's method.
     * @param step_limit How many steps to calculate
     */
    CMyVektor solve_euler(double x_start, double x_end, int step_limit, const CMyVektor & y_start) {
        const double h = (x_end - x_start) / step_limit;

        CMyVektor y = y_start;
        double x = x_start;

        for (int count = 0; count < step_limit; count++) {
            std::cout << "Step " << count << ":\n";
            std::cout << "\tx = " << x << "\n";
            std::cout << "\ty = " << y << "\n";

            CMyVektor y_d = ableitungen(y, x);

            std::cout << "\ty' = " << y_d << "\n\n";

            CMyVektor y_new = y + h * y_d;

            y = y_new;
            x += h;
        }

        return y;

    }


    /**
     * Find the approximate y(x_end) using Heun's method.
     * @param step_limit How many steps to calculate
     */
    CMyVektor solve_heun(double x_start, double x_end, int step_limit, const CMyVektor & y_start) {
        const double h = (x_end - x_start) / step_limit;

        CMyVektor y = y_start;
        double x = x_start;

        for (int count = 0; count < step_limit; count++) {
            double x_next = x + h;
            CMyVektor y_d_euler = ableitungen(y, x);
            CMyVektor y_euler = y + h * y_d_euler;
            CMyVektor y_d_heun = 0.5 * (ableitungen(y, x) + ableitungen(y_euler, x_next));
            CMyVektor y_heun = y + h * y_d_heun;

            std::cout << "Step " << count << ":\n";
            std::cout << "\tx = " << x << "\n";
            std::cout << "\ty = " << y << "\n";
            std::cout << "\ty'_orig (y' euler) = " << y_d_euler << "\n\n";

            std::cout << "\ty_test (y euler) = " << y_euler << "\n";
            std::cout << "\ty'_test (y' euler next) = " << ableitungen(y_euler, x_next) << "\n\n";

            std::cout << "\ty'_mittel (y' heun) = " << y_d_heun << "\n";
            std::cout << "\ty_next (y heun) = " << y_d_heun << "\n\n";

            y = y_heun;
            x = x_next;

        }

        return y;

    }

};

#endif //P3_C_DGLSOLVER_H
