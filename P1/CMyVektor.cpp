#include "exception.h"
#include <vector>
#include <cmath>

#ifndef CMYVEKTOR
#define CMYVEKTOR

class CMyVektor {
public:
    CMyVektor(std::vector<double> value) {
        set_dimension(value.size());

        set_value(value);
    }


    CMyVektor(int dimension) {
        set_dimension(dimension);
        this->value.resize(dimension);
    }


    CMyVektor(int dimension, std::vector<double> value) {
        set_dimension(dimension);

        set_value(value);
    }


    /**
     * Set new values to this vector as a double array.
     * If passed array's size is bigger than dimension, the rest will be omitted
     * If passed array's size is smaller than dimension, throw WrongDimensionException
     *
     * @param value This vector's new value
     */
    void set_value(std::vector<double> value) {
        if (this->dimension > value.size()) {
            throw VectorException::WrongDimensionException();
        }
        this->value = value;
        this->is_value_set = true;
    }


    /**
     * Set one value in one certain position
     * @param index target's index
     * @param new_value target's new value
     */
    void set_value_at(int index, double new_value) {
        this->validate_value();
        this->value[index] = new_value;
    }


    /**
     * Set this vector's dimension. Also reset this vector's value
     * @param dimension This vector's new dimension
     */
    void set_dimension(int dimension) {
        validate_dimension(dimension);

        this->dimension = dimension;
        this->value.resize(dimension);
        this->reset_value();
    }


    /**
     * @return This vector's dimension
     */
    int get_dimension() const {
        return this->dimension;
    }


    /**
 * @return this vector's value
 */
    std::vector<double> get_value() const {
        return this->value;
    }


    /**
     * @return This vector's length
     */
    double get_length() const {
        double square_sum = 0;
        for (int i = 0; i < dimension; i++) {
            square_sum += value[i] * value[i];
        }

        return sqrt(square_sum);
    }


    double operator[] (const int i) const {
        this->validate_value();

        return this->value[i];
    }


private:
    std::vector<double> value;
    int dimension;
    bool is_value_set = false;


    void reset_value() {
        this->value.clear();
        this->is_value_set = false;
    }


    /**
     * Throw EmptyValueException if this vector's value is not defined
     */
    void validate_value() const {
        if (!is_value_set) {
            throw VectorException::EmptyValueException();
        }
    }


    /**
     * Throw InvalidDimensionException if dimension is not valid
     * @param dimension
     */
    void validate_dimension(int dimension) const {
        if (dimension < 1)
            throw VectorException::InvalidDimensionException();
    }
};

CMyVektor operator+ (CMyVektor vector_a, CMyVektor vector_b) {
    if (vector_a.get_dimension() != vector_b.get_dimension()) {
        throw VectorException::InvalidDimensionException();
    }

    std::vector<double> new_value;
    int new_dimension = vector_a.get_dimension();

    new_value.resize(new_dimension);

    for (int i = 0; i < new_dimension; i++) {
        new_value[i] = vector_a[i] + vector_b[i];
    }

    return CMyVektor(new_dimension, new_value);
}


/**
 * Calculate scalar product of a vector and a real number
 * @param vector
 * @param LAMBDA
 * @return
 */
CMyVektor operator* (CMyVektor vector, const double LAMBDA) {
    for (int i = 0; i < vector.get_dimension(); i++) {
        vector.set_value_at(i, LAMBDA * vector[i]);
    }

    return vector;
}


/**
 * Calculate scalar product of a vector and a real number
 * @param LAMBDA
 * @param vector
 * @return
 */
CMyVektor operator* (const double LAMBDA, CMyVektor vector) {
    return vector * LAMBDA;
}


std::ostream& operator<< (std::ostream & out, const CMyVektor vector) {
    out << "( ";
    for (int i = 0; i < vector.get_dimension() - 1; i++) {
        out << vector[i] << " , ";
    }
    out << vector[vector.get_dimension() - 1] << " )";

    return out;
}


CMyVektor gradient(CMyVektor x, double (*function)(CMyVektor)) {
    const double h = 1e-8;

    double f2 = function(x);

    std::vector<double> gradient_value;

    gradient_value.resize(x.get_dimension());

    for (int i = 0; i < gradient_value.size(); i++) {
        CMyVektor x_h = x;
        x_h.set_value_at(i, x_h[i] + h);
        double f1 = function(x_h);
        gradient_value[i] = (f1 - f2) / h;
    }

    return CMyVektor(gradient_value);
}

#endif