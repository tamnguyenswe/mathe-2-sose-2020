#include <iostream>
#include <vector>
#include <cmath>
#include "CMyVektor.cpp"

CMyVektor find_max(CMyVektor x, double (*f)(CMyVektor), double lambda = 1) {
    int loop_count = 0;
    const double GRAD_LIMIT = 1e-5;

    while ((gradient(x, f).get_length() >= GRAD_LIMIT) &&
           (loop_count < 25)) {
        CMyVektor x_new = x + lambda * gradient(x, f);

        std::cout << "Step " << loop_count << ":\n";
        std::cout << "  x = " << x << "\n";
        std::cout << "  f(x) = " << f(x) << "\n";
        std::cout << "  lambda = " << lambda << "\n";
        std::cout << "  grad f(x) = " << gradient(x, f) << "\n";
        std::cout << "  |grad f(x)| = " << gradient(x, f).get_length() << "\n\n";

        if (f(x_new) > (f(x))) {
            CMyVektor x_test = x + 2 * lambda * gradient(x, f);

            if (f(x_test) > f(x_new)) {
                x = x_test;
                lambda *= 2;
                std::cout << "  lambda = lambda x 2 = " << lambda << "\n";
                std::cout << "  x_test = " << x_test << "\n";
            } else {
                x = x_new;
                std::cout << "  lambda = lambda (" << lambda << ")\n";
                std::cout << "  x_new = " << x_new<< "\n";
            }
        } else {
            while (f(x_new) <= f(x)) {
                lambda /= 2;
                x_new = x + lambda * gradient(x, f);
                std::cout << "  lambda = lambda / 2 = " << lambda << "\n";
                std::cout << "  x_new = " << x_new << "\n";
                std::cout << "  f(x_new) = " << f(x_new) << "\n\n";
            }
            x = x_new;
        }

        std::cout << "\n\n";

        loop_count++;
    }

    return x;
}


double f(CMyVektor x) {
    if (x.get_dimension() != 2) {
        throw VectorException::WrongDimensionException();
    }

    return sin(x[0] * x[1]) + sin(x[0]) + cos(x[1]);
}


double g(CMyVektor x) {
    if (x.get_dimension() != 3) {
        throw VectorException::WrongDimensionException();
    }
    //2x1^2 - 2x1x2 + x2^2 + x3^3 - 2x1 - 4x3
    return -((2 * x[0] * x[0]) - (2 * x[0] * x[1]) + (x[1] * x[1]) + (x[2] * x[2]) - (2 * x[0]) - (4 * x[2]));
}


int main() {
//    std::vector<double> x_value = {0.2, -2.1};
    std::vector<double> x_value = {0, 0, 0};
    CMyVektor x = CMyVektor(x_value);

    CMyVektor x_max = find_max(x, g, 0.1);
    std::cout << x_max;



    return 0;
}
