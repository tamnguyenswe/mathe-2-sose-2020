//
// Created by presariohg on 09/06/2020.
//

#ifndef P5_CZUFALL_H
#define P5_CZUFALL_H
#include <cmath>
#include <vector>
#include <time.h>

class CZufall {
public:
    static int wert(int a, int b) {
        int range = abs(a - b);
        int random = rand() % (range + 1);

        return random + (int) std::fmin(a, b);
    }

    static void initialisiere(int n) {
        srand(n);
    }


    static std::vector<int> test(int a, int b, int n) {
        std::vector<int> frequency;
        int size = abs(a - b);
        int smaller = std::fmin(a, b);

        frequency.resize(size + 1);
        for (auto &elem : frequency)
            elem = 0;

        for (int i = 0; i < n; i++) {
//            frequency[wert(a, b) - smaller]++;
            frequency[wert(a, b) - smaller ]++;
        }

        return frequency;
    }

    static std::vector<int> test_false(int a, int b, int n) {
        std::vector<int> frequency;
        int size = abs(a - b);
        int smaller = std::fmin(a, b);

        frequency.resize(size);
        for (auto &elem : frequency)
            elem = 0;

        for (int i = 0; i < n; i++) {
            initialisiere(time(NULL));
            frequency[wert(a, b) - smaller]++;
        }

        return frequency;
    }
};


#endif //P5_CZUFALL_H
