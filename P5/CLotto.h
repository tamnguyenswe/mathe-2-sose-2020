//
// Created by presariohg on 09/06/2020.
//

#ifndef P5_CLOTTO_H
#define P5_CLOTTO_H
#include "CZufall.h"
#include <iostream>

class CLotto {
private:
    std::vector<int> ticket;

    std::vector<int> result;

    void draw() {
        int marker[50];
        this->result = {};

        for (auto &i : marker)
            i = 0;

        for (int i = 0; i < 6; i++) {
            int number = CZufall::wert(0, 49);

            while (marker[number] != 0)
                number = CZufall::wert(0, 49);

            marker[number]++;
            result.push_back(number);
        }
    }



public:
    CLotto(int seed) {
        if (seed < 0)
            CZufall::initialisiere(time(NULL));
        else
            CZufall::initialisiere(seed);
    }

    void set_ticket(std::vector<int> ticket) {
        //validate the ticket

        if (ticket.size() != 6) {
            std::cout << "INVALID TICKET! YOUR MUST CHOOSE 6 NUMBERS BETWEEN 0 AND 49!\n";
            exit(-1);
        }

        int marker[50];
        for (auto &i : marker)
            i = 0;

        for (auto number : ticket) {
            if (number < 0 || number > 49) {
                std::cout << "INVALID TICKET! " << number << " IS NOT A VALID NUMBER!\n";
                exit(-1);
            }

            if (marker[number] != 0) {
                std::cout << "INVALID TICKET! " << number << " IS DUPLICATED!\n";
                exit(-1);
            }
            marker[number]++;
        }

        this->ticket = ticket;
    }


    std::vector<int> get_ticket() {
        return this->ticket;
    }


    std::vector<int> get_result() {
        return this->result;
    }


    /**
     * @return A random valid ticket
     */
    std::vector<int> random_ticket() {
        int marker[50];
        std::vector<int> random_ticket = {};

        for (auto &i : marker)
            i = 0;

        for (int i = 0; i < 6; i++) {
            int number = CZufall::wert(0, 49);

            while (marker[number] != 0)
                number = CZufall::wert(0, 49);

            marker[number]++;
            random_ticket.push_back(number);
        }

        return random_ticket;
    }


    /**
     * Draw a new result and count how many matches with given ticket
     * @return A vector contain matched numbers
     */
    std::vector<int> compare_result(std::vector<int> ticket) {
        draw();
        std::vector<int> matches = {};

        for (auto number_ticket : ticket) {
            for (auto number_result : result) {
                if (number_ticket == number_result)
                    matches.push_back(number_ticket);
            }
        }

        return matches;
    }


    /**
     * @param fixed_ticket Each random result will be compared with this fixed ticket
     * @param pairs_number How exactly many pairs of number to be matched, min = 1, max = 6
     * @param times How many time to run this Monte Carlo simulation
     */
    void monte_carlo_fixed(std::vector<int> fixed_ticket, int pairs_number, int times) {
        int count = 0;

        for (int loop_count = 0; loop_count < times; loop_count++) {
            std::vector<int> matches = compare_result(fixed_ticket);

            if (matches.size() == pairs_number)
                count++;
        }

        std::cout << "Monte Carlo Simulation ran with fixed ticket : [ ";

        for (int number : fixed_ticket)
            std::cout << number << ' ';
        std::cout << "] " << times << " times, matched exactly " << pairs_number << " pairs ";
        std::cout << "for " << count << " times.\n";
    }


    void monte_carlo_changing(int pairs_number, int times) {
        int count = 0;
        for (int loop_count = 0; loop_count < times; loop_count++) {
            std::vector<int> matches = compare_result(random_ticket());

            if (matches.size() == pairs_number)
                count++;
        }

        std::cout << "Monte Carlo Simulation ran with random tickets ";
        std::cout << times << " times, matched exactly " << pairs_number << " pairs ";
        std::cout << "for " << count << " times.\n";
    }
};




#endif //P5_CLOTTO_H
